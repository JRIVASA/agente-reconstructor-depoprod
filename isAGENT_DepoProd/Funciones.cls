VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Funciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private Declare Function GetPrivateProfileString Lib "kernel32" Alias _
"GetPrivateProfileStringA" (ByVal lpApplicationName _
As String, ByVal lpKeyName As Any, ByVal lpDefault _
As String, ByVal lpReturnedString As String, ByVal _
nSize As Long, ByVal lpFileName As String) As Long

Private Declare Function WritePrivateProfileString Lib "kernel32" Alias _
"WritePrivateProfileStringA" (ByVal lpApplicationName _
As String, ByVal lpKeyName As Any, ByVal lpstring _
As Any, ByVal lpFileName As String) As Long

Public Function sGetIni(SIniFile As String, SSection As String, SKey _
As String, SDefault As String) As String
    
    Const MaxLen = 10000
    
    Dim sTemp As String * MaxLen
    Dim NLength As Integer
    
    sTemp = Space$(MaxLen)
    NLength = GetPrivateProfileString(SSection, SKey, SDefault, sTemp, MaxLen - 1, SIniFile)
    
    sGetIni = Left$(sTemp, NLength)
    
End Function

Public Function sWriteIni(SIniFile As String, SSection As String, SKey _
        As String, SData As String) As String
    Dim NLength As Integer
    
    NLength = WritePrivateProfileString(SSection, SKey, SData, SIniFile)
End Function

Public Function SafeCreateObject(pClass As String, Optional pServerName As String = "") As Object
    
    On Error GoTo Error
    
    If Trim(pServerName) = Empty Then
        Set SafeCreateObject = CreateObject(pClass)
    Else
        Set SafeCreateObject = CreateObject(pClass, pServerName)
    End If
    
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    
    Set SafeCreateObject = Nothing
    
End Function

Public Sub Ejecutar()
    
    If sGetIni((App.Path & "\Setup.ini"), "Opciones", "ActualizarDepoprodExistencia", "1") = "1" Then
        Call ActualizarDepoprod
    End If
    
    If sGetIni((App.Path & "\Setup.ini"), "Opciones", "ActualizarDepoprodCantidadOrdenada", "1") = "1" Then
        Call ActualizarCantidadOrdenada
    End If
    
    If sGetIni((App.Path & "\Setup.ini"), "Opciones", "ActualizarDepoprodCantidadComprometida", "1") = "1" Then
        Call ActualizarCantidadComprometida
    End If
    
End Sub

Public Sub ActualizarCantidadComprometida()
    
    On Error GoTo Error1
    
    Dim Rec_DP As New ADODB.Recordset
    Dim Rec_Prod As New ADODB.Recordset
    
    Dim mDeposito As String
    
    Dim Trans As Boolean, K, W, SQL, SQL1, SQL2, SQL3, SQL4
    
    Trans = False
    
    Call ConectarBDD
    
    'ENT.BDD.Open
    'SQL1 = " select sum(cantidad) as sumCantidad, sum(CantidadVendida) as sumCantidadVendida, producto, deposito from ( " & _
    " select tr.n_CANTIDAD as cantidad, tr.n_CANT_VENDIDA as cantidadVendida, tr.c_CODARTICULO as producto, ma.C_CODDEPOSITO as deposito from ma_ventas ma " & _
    " inner join TR_VENTAS tr on ma.c_DOCUMENTO = tr.c_DOCUMENTO and ma.c_CONCEPTO = 'PED' and ma.c_status = 'DPE' " & _
    " ) as t group by producto, deposito "
    
    SQL1 = _
    "SELECT ROUND(SUM(Cantidad), 8, 0) AS SumCantidad, " & _
    "ROUND(SUM(CantidadVendida), 8, 0) AS SumCantidadVendida, " & vbNewLine & _
    "Producto, Deposito FROM ( " & vbNewLine & _
    "    SELECT TR.n_Cantidad AS Cantidad, TR.n_Cant_Vendida AS CantidadVendida, " & vbNewLine & _
    "    TR.c_CodArticulo AS Producto, MA.c_CodDeposito AS Deposito " & vbNewLine & _
    "    FROM MA_VENTAS MA " & _
    "    INNER JOIN TR_VENTAS TR " & vbNewLine & _
    "    ON MA.c_Documento COLLATE MODERN_SPANISH_CI_AS = TR.c_Documento COLLATE MODERN_SPANISH_CI_AS " & vbNewLine & _
    "    AND TR.c_Concepto COLLATE MODERN_SPANISH_CI_AS = MA.c_Concepto COLLATE MODERN_SPANISH_CI_AS " & vbNewLine & _
    "    AND MA.c_Concepto COLLATE MODERN_SPANISH_CI_AS = 'PED' COLLATE MODERN_SPANISH_CI_AS " & _
    "    AND MA.c_Status COLLATE MODERN_SPANISH_CI_AS = 'DPE' COLLATE MODERN_SPANISH_CI_AS " & vbNewLine & _
    "    UNION ALL " & vbNewLine & _
    "    SELECT 0 AS Cantidad, 0 AS CantidadVendida, " & vbNewLine & _
    "    PRO.c_Codigo AS Producto, DEP.c_CodDeposito AS Deposito " & vbNewLine & _
    "    FROM MA_PRODUCTOS PRO " & _
    "    CROSS JOIN MA_DEPOSITO DEP " & vbNewLine & _
    ") AS TB GROUP BY Producto, Deposito " & vbNewLine
    
    'Debug.Print SQL1
    ENT.BDD.CommandTimeout = 0
    
    'Rec_Prod.Open sql1, ENT.BDD, adOpenStatic, adLockReadOnly
    
    ENT.BDD.BeginTrans
    Trans = True
    
'    While Not Rec_Prod.EOF
'
'        Rec_DP.Open "SELECT * FROM MA_DEPOPROD " & vbNewLine & _
'        "WHERE c_CodArticulo = '" & Rec_Prod!Producto & "' " & vbNewLine & _
'        "AND c_CodDeposito = '" & Rec_Prod!Deposito & "' " & vbNewLine, _
'        ENT.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
'
'        If Not Rec_DP.EOF Then
'            Rec_DP.Update
'                Rec_DP!n_Cant_Comprometida = Rec_Prod!SumCantidadVendida
'                W = W + 1
'        Else
'            If Rec_Prod!SumCantidadVendida <> 0 Then
'                Rec_DP.AddNew
'
'                    Rec_DP!C_CODDEPOSITO = Rec_Prod!Deposito
'                    Rec_DP!c_CodArticulo = Trim(Rec_Prod!Producto)
'                    Rec_DP!n_Cantidad = CDbl(0)
'                    Rec_DP!n_Cant_Comprometida = Trim(Rec_Prod!SumCantidadVendida)
'                    Rec_DP!C_DESCRIPCION = vbNullString
'                    K = K + 1
'            End If
'        End If
'
'        'Me.Caption = K & " - " & W
'        Rec_DP.UpdateBatch
'        Rec_DP.Close
'
'        Rec_Prod.MoveNext
'        DoEvents
'
'    Wend
    
    ENT.BDD.Execute _
    "UPDATE MA_DEPOPROD SET " & _
    "n_Cant_Comprometida = 0 ", K ' Primero limpiar lo viejo, borron y Cuenta Nueva.
    
    SQL2 = _
    ";WITH RebuildData AS ( " & vbNewLine & _
    SQL1 & vbNewLine & _
    ") " & vbNewLine & _
    "INSERT INTO MA_DEPOPROD (c_CodDeposito, c_CodArticulo, c_Descripcion, " & vbNewLine & _
    "n_Cantidad, n_Cant_Comprometida, n_Cant_Ordenada) " & vbNewLine & _
    "SELECT RD.Deposito AS c_CodDeposito, RD.Producto AS c_CodArticulo, " & vbNewLine & _
    "'' AS c_Descripcion, 0 AS n_Cantidad, " & vbNewLine & _
    "RD.SumCantidadVendida AS n_Cant_Comprometida, 0 AS n_Cant_Ordenada " & vbNewLine & _
    "FROM RebuildData RD " & _
    "LEFT JOIN MA_DEPOPROD DP " & vbNewLine & _
    "ON RD.Deposito COLLATE MODERN_SPANISH_CI_AS = DP.c_CodDeposito COLLATE MODERN_SPANISH_CI_AS " & vbNewLine & _
    "AND RD.Producto COLLATE MODERN_SPANISH_CI_AS = DP.c_CodArticulo COLLATE MODERN_SPANISH_CI_AS " & vbNewLine & _
    "WHERE DP.n_Cantidad IS NULL " & vbNewLine & _
    "AND RD.SumCantidadVendida <> 0 " & vbNewLine
    
    ENT.BDD.Execute SQL2, K ' Ingresados
    
    If ExisteTablaV3("MA_LOTE_GESTION_TRANSFERENCIA_PICKING", ENT.BDD) Then
        
        mDeposito = BuscarValorBD("Valor", _
        "SELECT Valor FROM MA_REGLASDENEGOCIO " & _
        "WHERE Campo LIKE 'TRA_Consola_CodDepositoOrigen' ", Empty, ENT.BDD)
        
        If Trim(mDeposito) = Empty Then
            
            ENT.BDD.RollbackTrans
            Trans = False
            
            LogContent = FormatDateTime(Now, vbGeneralDate) & _
            " - Error al actualizar Cantidad Comprometida (Pedidos): " & _
            "El c�digo de Dep�sito CENDIS / Consola de Transferencia no est� establecido o es inv�lido. "
            
            LogFile LogContent
            
            Exit Sub
            
        End If
        
        SQL3 = _
        "SELECT '" & mDeposito & "' AS CodDeposito, PP.CodProducto AS c_CodArticulo, PRO.c_Descri, " & _
        "0 AS n_Cantidad, ROUND(SUM(PP.CantRecolectada), 8, 0) AS CantRecolectada, " & _
        "0 AS n_Cant_Ordenada " & _
        "FROM MA_LOTE_GESTION_TRANSFERENCIA_PICKING PP " & _
        "INNER JOIN MA_LOTE_GESTION_TRANSFERENCIA MA " & _
        "ON PP.CodLote = MA.cs_Corrida " & _
        "INNER JOIN MA_PRODUCTOS PRO " & _
        "ON PP.CodProducto = PRO.c_Codigo " & _
        "WHERE MA.b_Anulada = 0 " & _
        "AND MA.b_Finalizada = 0 " & _
        "AND PP.Picking = 1 " & _
        "AND PP.CantRecolectada > 0 " & _
        "GROUP BY PP.CodProducto, PRO.c_Descri "
        
        SQL4 = _
        ";WITH RebuildData AS ( " & vbNewLine & _
        SQL3 & vbNewLine & _
        ") " & vbNewLine & _
        "INSERT INTO MA_DEPOPROD (c_CodDeposito, c_CodArticulo, c_Descripcion, " & vbNewLine & _
        "n_Cantidad, n_Cant_Comprometida, n_Cant_Ordenada) " & vbNewLine & _
        "SELECT RD.CodDeposito AS c_CodDeposito, RD.c_CodArticulo AS c_CodArticulo, " & vbNewLine & _
        "'' AS c_Descripcion, 0 AS n_Cantidad, " & vbNewLine & _
        "0 AS n_Cant_Comprometida, 0 AS n_Cant_Ordenada " & vbNewLine & _
        "FROM RebuildData RD " & _
        "LEFT JOIN MA_DEPOPROD DP " & vbNewLine & _
        "ON RD.CodDeposito COLLATE MODERN_SPANISH_CI_AS = DP.c_CodDeposito COLLATE MODERN_SPANISH_CI_AS " & vbNewLine & _
        "AND RD.c_CodArticulo COLLATE MODERN_SPANISH_CI_AS = DP.c_CodArticulo COLLATE MODERN_SPANISH_CI_AS " & vbNewLine & _
        "WHERE DP.n_Cantidad IS NULL " & vbNewLine
        
        ENT.BDD.Execute SQL4, K ' Ingresados
        
    End If
    
    SQL2 = _
    ";WITH RebuildData AS ( " & vbNewLine & _
    SQL1 & vbNewLine & _
    ") " & vbNewLine & _
    "UPDATE MA_DEPOPROD SET " & vbNewLine & _
    "n_Cant_Comprometida = RD.SumCantidadVendida " & vbNewLine & _
    "FROM RebuildData RD INNER JOIN MA_DEPOPROD DP " & vbNewLine & _
    "ON RD.Deposito COLLATE MODERN_SPANISH_CI_AS = DP.c_CodDeposito COLLATE MODERN_SPANISH_CI_AS " & vbNewLine & _
    "AND RD.Producto COLLATE MODERN_SPANISH_CI_AS = DP.c_CodArticulo COLLATE MODERN_SPANISH_CI_AS " & vbNewLine
    
    ENT.BDD.Execute SQL2, W ' Actualizados
    
    If ExisteTablaV3("MA_LOTE_GESTION_TRANSFERENCIA_PICKING", ENT.BDD) Then
        
        SQL3 = _
        "SELECT '" & mDeposito & "' AS CodDeposito, PP.CodProducto AS c_CodArticulo, PRO.c_Descri, " & _
        "0 AS n_Cantidad, ROUND(SUM(PP.CantRecolectada), 8, 0) AS CantRecolectada, " & _
        "0 AS n_Cant_Ordenada " & _
        "FROM MA_LOTE_GESTION_TRANSFERENCIA_PICKING PP " & _
        "INNER JOIN MA_LOTE_GESTION_TRANSFERENCIA MA " & _
        "ON PP.CodLote = MA.cs_Corrida " & _
        "INNER JOIN MA_PRODUCTOS PRO " & _
        "ON PP.CodProducto = PRO.c_Codigo " & _
        "WHERE MA.b_Anulada = 0 " & _
        "AND MA.b_Finalizada = 0 " & _
        "AND PP.Picking = 1 " & _
        "AND PP.CantRecolectada > 0 " & _
        "GROUP BY PP.CodProducto, PRO.c_Descri "
        
        SQL4 = _
        ";WITH RebuildData AS ( " & vbNewLine & _
        SQL3 & vbNewLine & _
        ") " & vbNewLine & _
        "UPDATE MA_DEPOPROD SET " & vbNewLine & _
        "n_Cant_Comprometida = ROUND(n_Cant_Comprometida + RD.CantRecolectada, 8, 0) " & vbNewLine & _
        "FROM RebuildData RD " & _
        "INNER JOIN MA_DEPOPROD DP " & vbNewLine & _
        "ON RD.CodDeposito COLLATE MODERN_SPANISH_CI_AS = DP.c_CodDeposito COLLATE MODERN_SPANISH_CI_AS " & vbNewLine & _
        "AND RD.c_CodArticulo COLLATE MODERN_SPANISH_CI_AS = DP.c_CodArticulo COLLATE MODERN_SPANISH_CI_AS " & vbNewLine
        
        ENT.BDD.Execute SQL4, K ' Actualizados
        
    End If
    
    ENT.BDD.CommitTrans
    Trans = False
    
    Exit Sub
    
Error1:
    
    If Trans Then
        ENT.BDD.RollbackTrans
        Trans = False
    End If
    
    LogContent = FormatDateTime(Now, vbGeneralDate) & _
    " - Error al actualizar Cantidad Comprometida (Pedidos): " & _
    Err.Description & " " & "(" & Err.Number & ")[" & Err.Source & "]"
    
    LogFile LogContent
    
End Sub

Public Sub ActualizarCantidadOrdenada()
    
    'On Error GoTo error1
    Dim RecDepoProd As New ADODB.Recordset
    Dim RecCantidadOrdenada As New ADODB.Recordset
    
    Dim Trans As Boolean
    
    Trans = False
    
    Call ConectarBDD
    'ENT.BDD.Open
    
    CriterioVencimiento = sGetIni((App.Path & "\setup.ini"), "Opciones", "CriterioVencimiento", "Todos")
    
    Select Case CriterioVencimiento
    
        Case "Todos"
            SqlWhereVencimiento = ""
        Case "SoloVencidas"
            SqlWhereVencimiento = " AND ((MA.du_FechaVencimiento < GetDate() AND MA.du_FechaVencimiento <> CAST('19000101' AS DATE)) "
        Case "VigentesyNoAplicaVencimiento"
            SqlWhereVencimiento = " AND (((MA.du_FechaVencimiento >= GetDate() AND MA.du_FechaVencimiento <> CAST('19000101' AS DATE)) OR (MA.du_FechaVencimiento = CAST('19000101' AS DATE))) "
        Case "SoloVigentes"
            SqlWhereVencimiento = " AND ((MA.du_FechaVencimiento >= GetDate() AND MA.du_FechaVencimiento <> CAST('19000101' AS DATE)) "
        Case "VencidasyNoAplicaVencimiento"
            SqlWhereVencimiento = " AND (((MA.du_FechaVencimiento < GetDate() AND MA.du_FechaVencimiento <> CAST('19000101' AS DATE)) OR (MA.du_FechaVencimiento = CAST('19000101' AS DATE))) "
        Case Else
            SqlWhereVencimiento = ""
    
    End Select
    
    CriterioBackOrder = sGetIni((App.Path & "\setup.ini"), "Opciones", "CriterioBackOrder", "Todos")
    
    Select Case CriterioBackOrder
    
        Case "Todos"
            SqlWhereBackOrder = ""
        Case "SoloBackOrder"
            SqlWhereBackOrder = " AND MA.B_BackOrder = 1 "
        Case "SinBackOrder"
            SqlWhereBackOrder = " AND MA.B_BackOrder = 0 "
        Case Else
            SqlWhereBackOrder = ""
    
    End Select
    
    CriterioStatus = sGetIni((App.Path & "\setup.ini"), "Opciones", "CriterioStatus", "CON(DPE)")
    
    Select Case CriterioStatus
    
        Case "Todos"
            SqlWhereStatus = ""
        Case Else
            Debug.Print InStr(CriterioStatus, "CON(")
            If InStr(CriterioStatus, "CON(") > 0 Then
            
                CriterioStatus = Replace(CriterioStatus, "CON(", "")
                CriterioStatus = Replace(CriterioStatus, ")", "")
                
                ArrStatus = Split(CriterioStatus, "|")
                
                SqlWhereStatus = " AND MA.c_Status COLLATE MODERN_SPANISH_CI_AS IN ("
                SqlTempDataIn = ""
                
                For i = 0 To UBound(ArrStatus)
            
                    SqlTempDataIn = SqlTempDataIn & IIf(i < UBound(ArrStatus), "'" & ArrStatus(i) & "', ", "'" & ArrStatus(i) & "'")
                
                Next i
                
                SqlWhereStatus = SqlWhereStatus & SqlTempDataIn & ")"
            
            ElseIf InStr(CriterioStatus, "SIN(") > 0 Then
            
                CriterioStatus = Replace(CriterioStatus, "SIN(", "")
                CriterioStatus = Replace(CriterioStatus, ")", "")
                
                ArrStatus = Split(CriterioStatus, "|")
                
                SqlWhereStatus = " AND MA.c_Status COLLATE MODERN_SPANISH_CI_AS NOT IN ("
                SqlTempDataIn = ""
                
                For i = 0 To UBound(ArrStatus)
            
                    SqlTempDataIn = SqlTempDataIn & IIf(i < UBound(ArrStatus), "'" & ArrStatus(i) & "', ", "'" & ArrStatus(i) & "'")
                
                Next i
                
                SqlWhereStatus = SqlWhereStatus & SqlTempDataIn & ")"
                
            Else
            
                SqlWhereStatus = ""
            
            End If
    
    End Select
    
    CriterioFechaInicio = sGetIni((App.Path & "\setup.ini"), "Opciones", "CriterioFechaInicio", "Todos")
    
    If IsDate(CriterioFechaInicio) Then
        SqlWhereFechaInit = " AND MA.d_Fecha >= CAST('" & CriterioFechaInicio & "' AS DATE) "
    Else
        SqlWhereFechaInit = ""
    End If
    
    CriterioFechaFin = sGetIni((App.Path & "\setup.ini"), "Opciones", "CriterioFechaFin", "Todos")
    
    If IsDate(CriterioFechaFin) Then
        SqlWhereFechaFin = " AND MA.d_Fecha <= CAST('" & CriterioFechaFin & "' AS DATE) "
    Else
        SqlWhereFechaFin = ""
    End If
    
    'sql = "SELECT Producto, Deposito, CantODC, CantOdcRec, CantINV, CantNdc, " & _
    "CantOrdenada = case when (CantODC - CantINV - CantNdc) < 0 then 0 else (CantODC - CantINV - CantNdc) end FROM( " & _
    "SELECT Producto, Deposito, sum(CantODC) as CantODC, sum(CantOdcRec) as CantOdcRec, " & _
    "sum(CantINV) as CantINV, 0 as CantNdc FROM( " & _
    "SELECT ODC.c_DOCUMENTO AS Documento, ODC.c_CODARTICULO AS Producto, " & _
    "MA.C_DESPACHAR AS Deposito, SUM(ODC.n_CANTIDAD) AS CantODC, " & _
    "SUM(n_CANT_RECIBIDA) AS CantOdcRec, isnull(INV.CANTINV,0) as CantINV " & _
    "FROM TR_ODC ODC INNER JOIN MA_ODC MA ON ODC.c_DOCUMENTO = MA.c_DOCUMENTO " & _
    "LEFT JOIN (SELECT c_CODARTICULO, c_DEPOSITO, C_DOCUMENTO_ORIGEN, SUM(N_CANTIDAD) AS CANTINV FROM TR_INVENTARIO I " & _
    "WHERE c_CONCEPTO = 'REC' AND c_tipodoc_origen = 'ODC' " & _
    "GROUP BY c_CODARTICULO, c_DEPOSITO, C_DOCUMENTO_ORIGEN) INV ON ODC.c_CODARTICULO = INV.c_CODARTICULO " & _
    "AND MA.C_DESPACHAR = INV.c_DEPOSITO AND ODC.C_DOCUMENTO = INV.C_DOCUMENTO_ORIGEN " & _
    "WHERE 1=1 " & SqlWhereVencimiento & SqlWhereBackOrder & SqlWhereStatus & SqlWhereFechaInit & SqlWhereFechaFin & _
    "GROUP BY ODC.c_CODARTICULO, ODC.c_DOCUMENTO, MA.C_DESPACHAR, CANTINV) CantRecibida " & _
    "group by Producto, Deposito " & _
    "Union " & _
    "(SELECT c_CODARTICULO as Producto, c_DEPOSITO as Deposito, 0 AS CantODC, " & _
    "0 AS CantOdcRec, 0 AS CantINV, SUM(N_CANTIDAD) as CantNDC " & _
    "FROM TR_INVENTARIO WHERE c_CONCEPTO = 'NDC' " & _
    "GROUP BY c_CODARTICULO, c_DEPOSITO)) CantOrdenadaFinal " & _
    "order by Producto, Deposito"
    
    SQL1 = _
    "SELECT Producto, Deposito, CantODC, CantOdcRec, CantINV, CantNdc, CantOrdenada =  " & vbNewLine & _
    "ROUND(CASE WHEN (CantODC - CantINV - CantNdc) < 0 THEN 0 ELSE (CantODC - CantINV - CantNdc) END, 8, 0) " & vbNewLine & _
    "FROM ( " & vbNewLine & _
    "    SELECT Producto, Deposito, SUM(CantODC) as CantODC, SUM(CantOdcRec) as CantOdcRec, " & vbNewLine & _
    "    SUM(CantINV) AS CantINV, 0 AS CantNdc FROM ( " & vbNewLine & _
    "        SELECT ODC.c_Documento AS Documento, ODC.c_CodArticulo AS Producto, " & vbNewLine & _
    "        MA.c_Despachar AS Deposito, SUM(ODC.n_Cantidad) AS CantODC, " & vbNewLine & _
    "        SUM(n_Cant_Recibida) AS CantOdcRec, isNULL(INV.CantInv, 0) AS CantINV " & vbNewLine & _
    "        FROM TR_ODC ODC INNER JOIN MA_ODC MA " & vbNewLine & _
    "        ON ODC.c_Documento COLLATE MODERN_SPANISH_CI_AS = MA.c_Documento COLLATE MODERN_SPANISH_CI_AS " & vbNewLine & _
    "        LEFT JOIN (SELECT c_CodArticulo, c_Deposito, c_Documento_Origen, " & vbNewLine & _
    "        SUM(n_Cantidad) AS CantINV FROM TR_INVENTARIO I " & vbNewLine & _
    "        WHERE c_Concepto = 'REC' AND c_Tipodoc_Origen = 'ODC' " & vbNewLine & _
    "        GROUP BY c_CodArticulo, c_Deposito, c_Documento_Origen) INV " & vbNewLine & _
    "        ON ODC.c_CodArticulo COLLATE MODERN_SPANISH_CI_AS = INV.c_CodArticulo COLLATE MODERN_SPANISH_CI_AS " & vbNewLine & _
    "        AND MA.c_Despachar COLLATE MODERN_SPANISH_CI_AS = INV.c_Deposito COLLATE MODERN_SPANISH_CI_AS " & vbNewLine & _
    "        AND ODC.c_Documento COLLATE MODERN_SPANISH_CI_AS = INV.c_Documento_Origen COLLATE MODERN_SPANISH_CI_AS " & vbNewLine & _
    "        WHERE 1 = 1 " & SqlWhereVencimiento & SqlWhereBackOrder & SqlWhereStatus & SqlWhereFechaInit & SqlWhereFechaFin & vbNewLine & _
    "        GROUP BY ODC.c_CodArticulo, ODC.c_Documento, " & vbNewLine & _
    "        MA.c_Despachar, CantINV " & vbNewLine & _
    "    ) CantRecibida GROUP BY Producto, Deposito " & vbNewLine
    
    SQL1 = SQL1 & _
    "    UNION ALL " & vbNewLine & _
    "    SELECT c_CodArticulo AS Producto, c_Deposito as Deposito, " & vbNewLine & _
    "    0 AS CantODC, 0 AS CantOdcRec, 0 AS CantINV, " & vbNewLine & _
    "    SUM(n_Cantidad) AS CantNDC FROM TR_INVENTARIO " & vbNewLine & _
    "    WHERE c_Concepto COLLATE MODERN_SPANISH_CI_AS = 'NDC' COLLATE MODERN_SPANISH_CI_AS GROUP BY c_CodArticulo, c_Deposito " & vbNewLine & _
    "    UNION ALL " & vbNewLine & _
    "    SELECT PRO.c_Codigo AS Producto, DEP.c_CodDeposito AS Deposito, " & vbNewLine & _
    "    0 AS CantODC, 0 AS CantOdcRec, 0 AS CantINV, 0 AS CantNDC " & vbNewLine & _
    "    FROM MA_PRODUCTOS PRO CROSS JOIN MA_DEPOSITO DEP " & vbNewLine & _
    ") CantOrdenadaFinal " & vbNewLine
    
    ''Debug.Print sql
    'ENT.BDD.CommandTimeout = 0
    'RecCantidadOrdenada.Open sql, ENT.BDD, adOpenStatic, adLockReadOnly
    
    W = 0
    K = 0
    
    ENT.BDD.BeginTrans
    
    Trans = True
    
    'While Not RecCantidadOrdenada.EOF
        
        'RecDepoProd.Open "SELECT * FROM MA_DEPOPROD WHERE C_CODARTICULO = '" & Trim(RecCantidadOrdenada!Producto) & "' and c_coddeposito = '" & RecCantidadOrdenada!Deposito & "'", _
        'ENT.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
        
        'If Not RecDepoProd.EOF Then
            'RecDepoProd.Update
                'RecDepoProd!n_Cant_Ordenada = RecCantidadOrdenada!CantOrdenada
                'W = W + 1
        'Else
            'RecDepoProd.AddNew
                'RecDepoProd!c_CodDeposito = RecCantidadOrdenada!Deposito
                'RecDepoProd!c_CodArticulo = Trim(RecCantidadOrdenada!Producto)
                'RecDepoProd!n_Cantidad = CDbl(0)
                'RecDepoProd!n_Cant_Ordenada = RecCantidadOrdenada!CantOrdenada
                'RecDepoProd!C_DESCRIPCION = BuscarValorBD("c_descri", "Select C_Descri from MA_PRODUCTOS WHERE C_CODIGO = '" & RecCantidadOrdenada!Producto & "'", "No se encontr� el Valor.")
                'K = K + 1
        'End If
        ''Me.Caption = K & " - " & W
        'RecDepoProd.UpdateBatch
        'RecDepoProd.Close
        
        'RecCantidadOrdenada.MoveNext
        
        'DoEvents
        
    'Wend
    
    SQL2 = _
    ";WITH RebuildData AS ( " & vbNewLine & _
    SQL1 & vbNewLine & _
    ") " & vbNewLine & _
    "INSERT INTO MA_DEPOPROD (c_CodDeposito, c_CodArticulo, c_Descripcion, " & vbNewLine & _
    "n_Cantidad, n_Cant_Comprometida, n_Cant_Ordenada) " & vbNewLine & _
    "SELECT RD.Deposito AS c_CodDeposito, RD.Producto AS c_CodArticulo, " & vbNewLine & _
    "'' AS c_Descripcion, 0 AS n_Cantidad, " & vbNewLine & _
    "0 AS n_Cant_Comprometida, RD.CantOrdenada AS n_Cant_Ordenada " & vbNewLine & _
    "FROM RebuildData RD LEFT JOIN MA_DEPOPROD DP " & vbNewLine & _
    "ON RD.Deposito COLLATE MODERN_SPANISH_CI_AS = DP.c_CodDeposito COLLATE MODERN_SPANISH_CI_AS " & vbNewLine & _
    "AND RD.Producto COLLATE MODERN_SPANISH_CI_AS = DP.c_CodArticulo COLLATE MODERN_SPANISH_CI_AS " & vbNewLine & _
    "WHERE DP.n_Cantidad IS NULL " & vbNewLine & _
    "AND RD.CantOrdenada <> 0 " & vbNewLine
    
    ENT.BDD.Execute SQL2, K ' Ingresados
    
    SQL2 = _
    ";WITH RebuildData AS ( " & vbNewLine & _
    SQL1 & vbNewLine & _
    ") " & vbNewLine & _
    "UPDATE MA_DEPOPROD SET " & vbNewLine & _
    "n_Cant_Ordenada = RD.CantOrdenada " & vbNewLine & _
    "FROM RebuildData RD INNER JOIN MA_DEPOPROD DP " & vbNewLine & _
    "ON RD.Deposito COLLATE MODERN_SPANISH_CI_AS = DP.c_CodDeposito COLLATE MODERN_SPANISH_CI_AS " & vbNewLine & _
    "AND RD.Producto COLLATE MODERN_SPANISH_CI_AS = DP.c_CodArticulo COLLATE MODERN_SPANISH_CI_AS " & vbNewLine
    
    ENT.BDD.Execute SQL2, W ' Actualizados
    
    ENT.BDD.CommitTrans
    
    Exit Sub
    
Error1:
    
    If Trans Then
        ENT.BDD.RollbackTrans
    End If
    
    'MsgBox "Ocurri� un error al actualizar, por favor," & vbNewLine & "Reporte lo siguiente: " & vbNewLine & Err.Description
    
    LogContent = FormatDateTime(Now, vbGeneralDate) & " - Error al Actualizar Cantidad Ordenada (ODC): " & Err.Description & " " & "(" & Err.Number & ")[" & Err.Source & "]"
    LogFile LogContent
    
End Sub

Public Sub ActualizarDepoprod()
    
    On Error GoTo Error1
    
    Dim Rec_DPxP As New ADODB.Recordset
    Dim Rec_Prod As New ADODB.Recordset
    
    Dim Trans As Boolean
    
    Trans = False
    
    Call ConectarBDD
    
    ''ENT.BDD.Open
    
    'SQL1 = " SELECT c_DEPOSITO, c_CODARTICULO, SUM(Expr1) AS CANTIDAD, C_DESCRI " & _
    " FROM (SELECT TR_INVENTARIO.c_DEPOSITO, TR_INVENTARIO.c_CODARTICULO, SUM(TR_INVENTARIO.n_CANTIDAD) AS Expr1, " & _
    " MA_PRODUCTOS.C_DESCRI " & _
    " FROM TR_INVENTARIO LEFT OUTER JOIN " & _
    " MA_PRODUCTOS ON TR_INVENTARIO.c_CODARTICULO = MA_PRODUCTOS.C_CODIGO " & _
    " WHERE (TR_INVENTARIO.c_TIPOMOV = N'cargo') AND (c_tipodoc_origen <> N'nde') and c_documento not like 'e%' " & _
    " GROUP BY TR_INVENTARIO.c_DEPOSITO, TR_INVENTARIO.c_CODARTICULO, MA_PRODUCTOS.C_DESCRI " & _
    " Union " & _
    " SELECT TR_INVENTARIO.c_DEPOSITO, TR_INVENTARIO.c_CODARTICULO, - 1 * SUM(TR_INVENTARIO.n_CANTIDAD) AS Expr1, " & _
    " MA_PRODUCTOS.C_DESCRI " & _
    " FROM TR_INVENTARIO LEFT OUTER JOIN " & _
    " MA_PRODUCTOS ON TR_INVENTARIO.c_CODARTICULO = MA_PRODUCTOS.C_CODIGO " & _
    " WHERE (TR_INVENTARIO.c_TIPOMOV = N'descargo') AND (c_tipodoc_origen <> N'nde') and c_documento not like 'e%' " & _
    " GROUP BY TR_INVENTARIO.c_DEPOSITO, TR_INVENTARIO.c_CODARTICULO, MA_PRODUCTOS.C_DESCRI) m " & _
    " GROUP BY c_DEPOSITO, c_CODARTICULO, C_DESCRI "
    
    SQL1 = "SELECT c_Deposito AS Deposito, c_CodArticulo AS Producto, " & vbNewLine & _
    "ROUND(SUM(n_Cantidad), 8, 0) AS Cantidad, c_Descri AS Descripcion " & vbNewLine & _
    "FROM ( " & vbNewLine & _
    "    SELECT INV.c_Deposito, INV.c_CodArticulo, " & vbNewLine & _
    "    SUM(CASE WHEN c_Concepto = 'NDE' THEN INV.n_Cant_Teorica ELSE INV.n_Cantidad END) AS n_Cantidad, PRO.c_Descri " & vbNewLine & _
    "    FROM TR_INVENTARIO INV LEFT JOIN MA_PRODUCTOS PRO " & vbNewLine & _
    "    ON INV.c_CodArticulo COLLATE MODERN_SPANISH_CI_AS = PRO.c_Codigo COLLATE MODERN_SPANISH_CI_AS " & vbNewLine & _
    "    WHERE (INV.c_TipoMov COLLATE MODERN_SPANISH_CI_AS = N'Cargo' COLLATE MODERN_SPANISH_CI_AS) " & vbNewLine & _
    "    AND NOT LEFT (INV.c_Documento COLLATE MODERN_SPANISH_CI_AS, 1) = 'E' COLLATE MODERN_SPANISH_CI_AS " & vbNewLine & _
    "    GROUP BY INV.c_Deposito, INV.c_CodArticulo, " & vbNewLine & _
    "    PRO.c_Descri " & vbNewLine

    SQL1 = SQL1 & _
    "    UNION ALL " & vbNewLine & _
    "    SELECT INV.c_Deposito, INV.c_CodArticulo, " & vbNewLine & _
    "    - 1 * SUM(CASE WHEN c_Concepto = 'NDE' THEN INV.n_Cant_Teorica ELSE INV.n_Cantidad END) AS n_Cantidad, PRO.c_Descri " & vbNewLine & _
    "    FROM TR_INVENTARIO INV LEFT JOIN MA_PRODUCTOS PRO " & vbNewLine & _
    "    ON INV.c_CodArticulo COLLATE MODERN_SPANISH_CI_AS = PRO.c_Codigo COLLATE MODERN_SPANISH_CI_AS " & vbNewLine & _
    "    WHERE (INV.c_TipoMov COLLATE MODERN_SPANISH_CI_AS = N'Descargo' COLLATE MODERN_SPANISH_CI_AS) " & vbNewLine & _
    "    AND NOT LEFT (INV.c_Documento COLLATE MODERN_SPANISH_CI_AS, 1) = 'E' COLLATE MODERN_SPANISH_CI_AS " & vbNewLine & _
    "    GROUP BY INV.c_Deposito, INV.c_CodArticulo, " & vbNewLine & _
    "    PRO.c_Descri " & vbNewLine & _
    "    UNION ALL " & vbNewLine & _
    "    SELECT DEP.c_CodDeposito AS c_Deposito, PRO.c_Codigo AS c_CodArticulo, " & vbNewLine & _
    "    0 AS n_Cantidad, PRO.c_Descri " & vbNewLine & _
    "    FROM MA_PRODUCTOS PRO CROSS JOIN MA_DEPOSITO DEP " & vbNewLine & _
    ") TB GROUP BY c_Deposito, c_CodArticulo, c_Descri " & vbNewLine
    
    'ORDER BY CASE WHEN ROUND(SUM(n_Cantidad), 8, 0) <> 0 THEN 1 ELSE 0 END DESC,
    'ROUND(SUM(n_Cantidad), 8, 0) DESC
    
    'Debug.Print SQL1
    ENT.BDD.CommandTimeout = 0
    
    'Rec_Prod.Open SQL1, ENT.BDD, adOpenStatic, adLockReadOnly
    
    Dim TInicio As Date, TFin As Date
    
    ENT.BDD.BeginTrans
    
    TInicio = Now
    
    Trans = True
    
    'ENT.BDD.Execute "UPDATE  ma_depoprod SET n_cantidad=0"
    
    'While Not Rec_Prod.EOF
        
        ''Call Apertura_Recordset(rec_dpxp)
        ''rec_dpxp.CursorLocation = adUseClient
        ''Debug.Print "select * FROM ma_depoprod WHERE c_codarticulo = '" & Trim(rec_PROD!c_CODARTICULO) & "' and c_coddeposito = '" & rec_PROD!c_DEPOSITO & "'"
        
        'Rec_DPxP.Open "select * FROM ma_depoprod WHERE c_codarticulo = '" & Trim(Rec_Prod!c_CodArticulo) & "' and c_coddeposito = '" & Rec_Prod!c_DEPOSITO & "'", _
        'ENT.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
        
        ''Set rec_dpxp.ActiveConnection = Nothing
        ''Debug.Print rec_dpxp.Source
        
        'If Not Rec_DPxP.EOF Then
            'Rec_DPxP.Update
                'Rec_DPxP!n_Cantidad = Rec_Prod!CANTIDAD
                'W = W + 1
        'Else
            'Rec_DPxP.AddNew
                'Rec_DPxP!c_CodDeposito = Rec_Prod!c_DEPOSITO
                'Rec_DPxP!c_CodArticulo = Trim(Rec_Prod!c_CodArticulo)
                'Rec_DPxP!n_Cantidad = Rec_Prod!CANTIDAD
                'Rec_DPxP!C_DESCRIPCION = Trim(Rec_Prod!C_DESCRI)
                'K = K + 1
        'End If
        
        ''Me.Caption = K & " - " & W
        'Rec_DPxP.UpdateBatch
        'Rec_DPxP.Close
        'Rec_Prod.MoveNext
        
        'DoEvents
        
    'Wend
    
    SQL2 = _
    ";WITH RebuildData AS ( " & vbNewLine & _
    SQL1 & vbNewLine & _
    ") " & vbNewLine & _
    "INSERT INTO MA_DEPOPROD (c_CodDeposito, c_CodArticulo, c_Descripcion, " & vbNewLine & _
    "n_Cantidad, n_Cant_Comprometida, n_Cant_Ordenada) " & vbNewLine & _
    "SELECT RD.Deposito AS c_CodDeposito, RD.Producto AS c_CodArticulo, " & vbNewLine & _
    "RD.Descripcion AS c_Descripcion, RD.Cantidad AS n_Cantidad, " & vbNewLine & _
    "0 AS n_Cant_Comprometida, 0 AS n_Cant_Ordenada " & vbNewLine & _
    "FROM RebuildData RD LEFT JOIN MA_DEPOPROD DP " & vbNewLine & _
    "ON RD.Deposito COLLATE MODERN_SPANISH_CI_AS = DP.c_CodDeposito COLLATE MODERN_SPANISH_CI_AS " & vbNewLine & _
    "AND RD.Producto COLLATE MODERN_SPANISH_CI_AS = DP.c_CodArticulo COLLATE MODERN_SPANISH_CI_AS " & vbNewLine & _
    "WHERE DP.n_Cantidad IS NULL " & vbNewLine & _
    "AND RD.Cantidad <> 0 " & vbNewLine
    
    ENT.BDD.Execute SQL2, K ' Ingresados
    
    SQL2 = _
    ";WITH RebuildData AS ( " & vbNewLine & _
    SQL1 & vbNewLine & _
    ") " & vbNewLine & _
    "UPDATE MA_DEPOPROD SET " & vbNewLine & _
    "n_Cantidad = RD.Cantidad " & vbNewLine & _
    "FROM RebuildData RD INNER JOIN MA_DEPOPROD DP " & vbNewLine & _
    "ON RD.Deposito COLLATE MODERN_SPANISH_CI_AS = DP.c_CodDeposito COLLATE MODERN_SPANISH_CI_AS " & vbNewLine & _
    "AND RD.Producto COLLATE MODERN_SPANISH_CI_AS = DP.c_CodArticulo COLLATE MODERN_SPANISH_CI_AS " & vbNewLine
    
    ENT.BDD.Execute SQL2, W ' Actualizados
    
    ENT.BDD.CommitTrans
    
    TFin = Now
    
    Debug.Print "Tiempo de actualizacion: " & FormatDateTime(CDate(TFin - TInicio), vbLongTime)
    
    'Call MsgBox("Proceso Concluido al " & Now() & vbCrLf & W & " Productos Actualizados.", vbOKOnly, "Reconstructor de DepoProd")
    
    Exit Sub
    
Error1:
    
    If Trans Then
        ENT.BDD.RollbackTrans
    End If
    
    LogContent = FormatDateTime(Now, vbGeneralDate) & " - Error al ActualizarDepoprod (Existencia): " & Err.Description & " " & "(" & Err.Number & ")[" & Err.Source & "]"
    LogFile LogContent
    
End Sub


Private Sub ConectarBDD()

    On Error GoTo ErrHandler
    
    Dim Setup As String
    
    Setup = App.Path & "\Setup.ini"
    
    Dim SRV_LOCAL As String, SRV_REMOTE As String, PROVIDER_LOCAL As String, _
    UserDB As String, UserPwd As String, NewUser As String, NewPassword As String, _
    Nombre_BD_ADM As String
    
    'BUSCAR VALORES
    
    SRV_LOCAL = sGetIni(Setup, "Server", "srv_local", "?")
    
    If SRV_LOCAL = "?" Then
        Call MsgBox("El servidor local no se encuentra configurado.", vbCritical, "Error")
        End
    End If
    
    SRV_REMOTE = sGetIni(Setup, "Server", "srv_remote", "?")
    
    If SRV_REMOTE = "?" Then
        Call MsgBox("El servidor remoto no se encuentra configurado.", vbCritical, "Error")
        End
    End If
    
    Nombre_BD_ADM = sGetIni(Setup, "Server", "BD_ADM", "VAD10")
    
    If Trim(Nombre_BD_ADM) = Empty Then
        Nombre_BD_ADM = "VAD10"
    End If
    
    PROVIDER_LOCAL = sGetIni(Setup, "Proveedor", "Proveedor", "?")
    
    If PROVIDER_LOCAL = "?" Then
        Call MsgBox("El proveedor de servicio local no se encuentra configurado.", vbCritical, "Error")
        End
    End If
    
    UserDB = sGetIni(Setup, "Server", "User", "?")
    
    If UserDB = "?" Then
        UserDB = "SA"
    End If
    
    UserPwd = sGetIni(Setup, "Server", "Password", "?")
    
    If UserPwd = "?" Then
        UserPwd = ""
    End If
    
    If Not (UCase(UserDB) = "SA" And Len(UserPwd) = 0) Then
        
        Dim mClsTmp As Object
        
        Set mClsTmp = SafeCreateObject("SQLSafeGuard.Service")
        
        If Not mClsTmp Is Nothing Then
            UserDB = mClsTmp.Decode(gCodProducto, gNombreProducto, gPK, UserDB)
            UserPwd = mClsTmp.Decode(gCodProducto, gNombreProducto, gPK, UserPwd)
        End If
        
    End If
    
Retry:
    
    'INICIO CONEXION
    
    SafeItemAssign ENT.BDD.Properties, "Prompt", adPromptNever
    
    If ENT.BDD.State = adStateOpen Then ENT.BDD.Close
    
    ENT.BDD.ConnectionString = "Provider= " & PROVIDER_LOCAL & ";" & _
    "Persist Security Info=True;" & _
    "User ID=" & UserDB & ";" & _
    "Password=" & UserPwd & ";" & _
    "Initial Catalog=" & Nombre_BD_ADM & ";" & _
    "Data Source=" & SRV_REMOTE & ";"
    
    ENT.BDD.CommandTimeout = 0
    
    ENT.BDD.Open
    
    ConectarBaseDatos = True
    
    Exit Sub
    
ErrHandler:
    
    mErrDesc = Err.Description
    mErrNumber = Err.Number
    mErrSrc = Err.Source
    
    If mErrNumber = -2147217843 Then
        
        Set mClsTmp = SafeCreateObject("SQLSafeGuard.Service")
        
        If mClsTmp Is Nothing Then GoTo UnhandledErr
        
        TmpVar = mClsTmp.RequestAccess(gCodProducto, gNombreProducto, gPK)
        
        If Not IsEmpty(TmpVar) Then
            UserDB = TmpVar(0): UserPwd = TmpVar(1)
            NewUser = TmpVar(2): NewPassword = TmpVar(3)
            sWriteIni Setup, "Server", "User", NewUser
            sWriteIni Setup, "Server", "Password", NewPassword
            Resume Retry
        End If
        
        Set mClsTmp = Nothing
        
    End If
    
UnhandledErr:
    
    'Call MsgBox(Err.Description, vbCritical, "Error")
    
    LogContent = FormatDateTime(Now, vbGeneralDate) & " - Error al conectar a la BD: " & mErrDesc & " " & "(" & mErrNumber & ")[" & mErrSrc & "]"
    LogFile LogContent
    End
    
End Sub

Public Function BuscarValorBD(Campo As String, SQL As String, _
Optional pDefault = "", _
Optional pCn As ADODB.Connection) As String
    
    On Error GoTo Err_Campo
    
    Dim mRs As New ADODB.Recordset
    
    mRs.Open SQL, IIf(pCn Is Nothing, ENT.BDD, pCn), adOpenStatic, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        
        BuscarValorBD = CStr(mRs.Fields(Campo).Value)
        
    Else
        
        BuscarValorBD = pDefault
        
    End If
    
    mRs.Close
    
    Exit Function
    
Err_Campo:
    
    'Debug.Print Err.Description
    
    Err.Clear
    
    BuscarValorBD = pDefault
    
End Function

Public Function mensaje(activo As Boolean, texto As String, Optional pVbmodal = True) As Boolean
    uno = activo
    On Error GoTo Falla_Local
    
    If Not frm_MENSAJERIA.Visible Then
        frm_MENSAJERIA.mensaje.Text = IIf(frm_MENSAJERIA.mensaje.Text <> "", frm_MENSAJERIA.mensaje.Text & vbNewLine & vbNewLine & texto, texto)
        If pVbmodal Then
            If Not frm_MENSAJERIA.Visible Then
                Retorno = False
                
                frm_MENSAJERIA.Show vbModal
                Set frm_MENSAJERIA = Nothing
            End If
        Else
            Retorno = False
            frm_MENSAJERIA.Show
            frm_MENSAJERIA.aceptar.Enabled = False
            Set frm_MENSAJERIA = Nothing
        End If
    End If
    mensaje = Retorno
Falla_Local:
End Function
